class SingleMeth
    def initialize(l)
        @l = l
    end
    def size
        @l
    end
end 
obj1 = SingleMeth.new(10)
def obj1.size
    @l = 0
end
puts(obj1.size)
obj2=SingleMeth.new(10)
puts(obj2.size)