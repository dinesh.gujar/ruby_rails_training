#Create a ruby block which takes an argument and multiply with 2
def test
  yield 2
end

test { |n| puts "#{n * 2}" }

#Implement a Proc which takes an argument and multiply with 2.Call this proc inside a loop
def my_proc(n)
  proc = Proc.new { |x| puts x * 2 }
  n.each do |i|
    proc.call(i)
  end
end

my_proc([2, 3, 4, 5])
#Implement a Lambda which takes an argument  and returns the output by multiply arg with 2. Call this lambda inside a loop and return modified array

def lambda_test(n)
  my_multiply = ->(p) { return p * 2 }
  n.map { |i| my_multiply.call(i) }
  puts n
end

lambda_test([4, 5, 6, 7, 8])

def map_test(n)
  n.map { |i| i << " Hello" }
  puts n
end

#map_test(["Vikas", "Rails Learning Team", "Ruby Learning Team"])
